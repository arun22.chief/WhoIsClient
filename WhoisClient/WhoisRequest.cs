﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net.Sockets;
using System.Text;

namespace WhoisClient
{
    /// <summary>
    /// Allows to make a whois query
    /// </summary>
    public class WhoisRequest
    {
        private string _domain,
                       _tld,
                       _targetServer,
                       _defaultTargetServer = "whois.arin.net"; // Can be modified;

        /// <summary>
        /// Url to query
        /// </summary>
        public string Domain { get { return _domain; } }

        /// <summary>
        /// Get or set the default server to query, if no one has returned any result
        /// </summary>
        public string DefaultTargetServer
        {
            get { return _defaultTargetServer; }
            set { _defaultTargetServer = value; }
        }

        /// <summary>
        /// Fired when a response is received from the server
        /// </summary>
        public event EventHandler<WhoisEventArgs> ResponseReceived;

        /// <summary>
        /// Fired when connection closed
        /// </summary>
        public event EventHandler ConnectionClosed;

        /// <summary>
        /// Initialize a new instance of the class
        /// </summary>
        /// <param name="url"></param>
        public WhoisRequest(string Domain)
        {
            Initialize(Domain);
        }

        /// <summary>
        /// Do job
        /// </summary>
        private void Initialize(string Domain)
        {
            if (!String.IsNullOrEmpty(Domain))
            {
                this._domain = Domain;

                if (this._domain.IndexOf(".") < this._domain.Length - 4)
                {
                    this._domain = this._domain.Substring(this._domain.IndexOf(".") + 1);
                }

                this._tld = this._domain.Substring(this._domain.LastIndexOf(".") + 1);
                try
                {
                    this._targetServer = WhoisServer.DictionaryWhoisServer[this._tld];
                }
                catch
                {
                    this.OnConnectionClosed();
                    return;
                }

                if (String.IsNullOrEmpty(this._targetServer))
                {
                    this._targetServer = this._defaultTargetServer;
                }
            }
        }

        /// <summary>
        /// Send the query
        /// </summary>
        public void GetResponse()
        {
            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += new DoWorkEventHandler(bw_DoWork);
            bw.RunWorkerAsync();
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            string response = DoRequest(this._targetServer);

            StringReader sr = new StringReader(response);

            if (response.Contains("Whois Server:"))
            {
                string newTargetServer = "";

                while (newTargetServer == "")
                {
                    string line = sr.ReadLine().Trim();

                    if (line.StartsWith("Whois Server:"))
                    {
                        newTargetServer = line.Substring(14);

                        DoRequest(newTargetServer);
                    }
                }
            }

            this.OnConnectionClosed();
        }

        private string DoRequest(string server)
        {
            WhoisEventArgs wea = new WhoisEventArgs(
                this._domain,
                this._tld,
                server,
                "Trying to connect to " + server + " (query : " + this._domain + ") ...\r\n"
            );

            this.OnResponseReceived(wea);

            string response = "";

            TcpClient client = null;

            try
            {
                client = new TcpClient(server, 43);
            }
            catch (Exception ex)
            {
                wea = new WhoisEventArgs(
                    this._domain,
                    this._targetServer,
                    this._targetServer,
                    "Connection to server " + server + " failed : \r\n" + 
                            ex.Message + "\r\n\r\n"
                );

                this.OnResponseReceived(wea);
            }

            if (client != null)
            {
                string formatedDomain = this._domain + "\r\n";
                byte[] byteUrl = Encoding.ASCII.GetBytes(formatedDomain);

                Stream s = client.GetStream();

                bool error = true;

                try
                {
                    wea = new WhoisEventArgs(
                        this._domain,
                        this._targetServer,
                        this._targetServer,
                        "Connection to server " + this._targetServer + 
                        " established. Executing query ...\r\n\r\n"
                    );

                    this.OnResponseReceived(wea);

                    s.Write(byteUrl, 0, formatedDomain.Length);

                    error = false;
                }
                catch (Exception ex)
                {
                    wea = new WhoisEventArgs(
                        this._domain,
                        this._targetServer,
                        this._targetServer,
                        "Unable to perform query : \r\n" + ex.Message + "\r\n\r\n"
                    );

                    this.OnResponseReceived(wea);
                }

                if (!error)
                {
                    StreamReader sr = new StreamReader(s, Encoding.ASCII);

                    response = sr.ReadToEnd() + "\r\n";

                    sr.Close();
                    sr.Dispose();
                    sr = null;

                    wea = new WhoisEventArgs(
                        this._domain,
                        this._targetServer,
                        this._targetServer,
                        response
                    );

                    this.OnResponseReceived(wea);
                }

                s.Close();
                s.Dispose();
                s = null;

                client.Close();
                client = null;
            }

            return response;
        }

        public void OnResponseReceived(WhoisEventArgs e)
        {
            if (this.ResponseReceived != null)
            {
                this.ResponseReceived(this, e);
            }
        }

        public void OnConnectionClosed()
        {
            if (this.ConnectionClosed != null)
            {
                this.ConnectionClosed(this, null);
            }
        }
    }
}