﻿namespace WhoisClient
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.buttonCheckWhois = new System.Windows.Forms.Button();
            this.textBoxDomains = new System.Windows.Forms.TextBox();
            this.dataGridViewWhoisResult = new System.Windows.Forms.DataGridView();
            this.ColumnDomain = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnResult = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWhoisResult)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonCheckWhois
            // 
            this.buttonCheckWhois.Location = new System.Drawing.Point(978, 154);
            this.buttonCheckWhois.Name = "buttonCheckWhois";
            this.buttonCheckWhois.Size = new System.Drawing.Size(179, 36);
            this.buttonCheckWhois.TabIndex = 0;
            this.buttonCheckWhois.Text = "Check Whois";
            this.buttonCheckWhois.UseVisualStyleBackColor = true;
            this.buttonCheckWhois.Click += new System.EventHandler(this.buttonCheckWhois_Click);
            // 
            // textBoxDomains
            // 
            this.textBoxDomains.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBoxDomains.Location = new System.Drawing.Point(0, 0);
            this.textBoxDomains.Multiline = true;
            this.textBoxDomains.Name = "textBoxDomains";
            this.textBoxDomains.Size = new System.Drawing.Size(1160, 148);
            this.textBoxDomains.TabIndex = 2;
            this.textBoxDomains.Text = "Eine Domain pro Zeile";
            // 
            // dataGridViewWhoisResult
            // 
            this.dataGridViewWhoisResult.AllowUserToAddRows = false;
            this.dataGridViewWhoisResult.AllowUserToDeleteRows = false;
            this.dataGridViewWhoisResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnDomain,
            this.ColumnResult});
            this.dataGridViewWhoisResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewWhoisResult.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewWhoisResult.Name = "dataGridViewWhoisResult";
            this.dataGridViewWhoisResult.RowHeadersVisible = false;
            this.dataGridViewWhoisResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridViewWhoisResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewWhoisResult.Size = new System.Drawing.Size(1160, 416);
            this.dataGridViewWhoisResult.TabIndex = 3;
            // 
            // ColumnDomain
            // 
            this.ColumnDomain.DataPropertyName = "Domain";
            this.ColumnDomain.HeaderText = "Domain";
            this.ColumnDomain.Name = "ColumnDomain";
            this.ColumnDomain.Width = 68;
            // 
            // ColumnResult
            // 
            this.ColumnResult.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnResult.DataPropertyName = "Result";
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnResult.DefaultCellStyle = dataGridViewCellStyle2;
            this.ColumnResult.HeaderText = "Result";
            this.ColumnResult.Name = "ColumnResult";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.textBoxDomains);
            this.splitContainer1.Panel1.Controls.Add(this.buttonCheckWhois);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dataGridViewWhoisResult);
            this.splitContainer1.Size = new System.Drawing.Size(1160, 619);
            this.splitContainer1.SplitterDistance = 199;
            this.splitContainer1.TabIndex = 4;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1160, 619);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Main";
            this.Text = "Whois Check";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWhoisResult)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonCheckWhois;
        private System.Windows.Forms.TextBox textBoxDomains;
        private System.Windows.Forms.DataGridView dataGridViewWhoisResult;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDomain;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnResult;
    }
}

