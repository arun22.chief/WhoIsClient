﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

namespace WhoisClient
{
    public partial class Main : Form
    {
        private List<WhoisResult> ListWhoisResult = new List<WhoisResult>();
        private BindingList<WhoisResult> BindingList;

        public Main()
        {
            InitializeComponent();
            this.BindingList = new BindingList<WhoisResult>(this.ListWhoisResult);
            this.dataGridViewWhoisResult.DataSource = this.BindingList;
        }

        private void buttonCheckWhois_Click(object sender, EventArgs e)
        {
            string Domains = this.textBoxDomains.Text;
            string[] ArrayDomain = Domains.Split(new string[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries);

            Thread thread = new Thread(new ParameterizedThreadStart(this.CheckWhois));
            thread.Start(ArrayDomain);
        }

        private void CheckWhois(object ObjectArrayDomain)
        {
            string[] ArrayDomain = (string[])ObjectArrayDomain;

            for (int i = 0; i < ArrayDomain.Length; i++)
            {
                WhoisRequest req = new WhoisRequest(ArrayDomain[i]);
                req.ResponseReceived += new EventHandler<WhoisEventArgs>(req_ResponseReceived);
                req.ConnectionClosed += new EventHandler(req_ConnectionClosed);
                req.GetResponse();
            }
        }

        private void req_ConnectionClosed(object sender, EventArgs e)
        {
            this.dataGridViewWhoisResult.Invoke(new Action(this.ResetBindingList));
        }

        private void req_ResponseReceived(object sender, WhoisEventArgs e)
        {
            WhoisResult whoisresult = this.ListWhoisResult.Find(w => w.Domain == e.Domain);
            if (whoisresult == null)
            {
                whoisresult = new WhoisResult();
                whoisresult.Domain = e.Domain;
                whoisresult.Result = e.Message;
                this.ListWhoisResult.Add(whoisresult);
            }
            else
            {
                whoisresult.Result = e.Message;
            }
        }

        private void ResetBindingList()
        {
            this.BindingList.ResetBindings();
        }
    }
}
