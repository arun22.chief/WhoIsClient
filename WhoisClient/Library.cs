﻿namespace WhoisClient
{
    public class WhoisResult
    {
        private string _Domain;
        private string _Result;

        public string Domain
        {
            get { return this._Domain; }
            set { this._Domain = value; }
        }

        public string Result
        {
            get { return this._Result; }
            set { this._Result = value; }
        }
    }
}
