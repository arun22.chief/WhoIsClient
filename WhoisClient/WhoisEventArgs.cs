﻿using System;

namespace WhoisClient
{
    /// <summary>
    /// Arguments for the whois system    
    /// </summary>
    public class WhoisEventArgs : EventArgs
    {
        private string _domain, _message, _tld, _targetServer;

        /// <summary>
        /// URL to query
        /// </summary>
        public string Domain { get { return this._domain; } }

        /// <summary>
        /// Message returned by the whois server
        /// </summary>
        public string Message { get { return this._message; } }

        /// <summary>
        /// TLD of the current query
        /// </summary>
        public string TLD { get { return this._tld; } }

        /// <summary>
        /// Hostname of the server queried
        /// </summary>
        public string TargetServer { get { return this._targetServer; } }

        /// <summary>
        /// Initialize a new instance of the class with the specified parameters
        /// </summary>
        /// <param name="url">Url to query</param>
        /// <param name="tld">Top Level Domain</param>
        /// <param name="targetServer">Server handling the query</param>
        /// <param name="message">Message linked to the event</param>
        public WhoisEventArgs
            (string domain, string tld, string targetServer, string message)
        {
            this._domain = domain;
            this._tld = tld;
            this._targetServer = targetServer;
            this._message = message;
        }
    }
}