﻿using System.Collections.Generic;

namespace WhoisClient
{
    public class WhoisServer
    {
        public static readonly Dictionary<string, string> DictionaryWhoisServer = new Dictionary<string, string>()
        {
            {"br.com","whois.centralnic.net"},
            {"cn.com","whois.centralnic.net"},
            {"de.com","whois.centralnic.net"},
            {"eu.com","whois.centralnic.net"},
            {"gb.com","whois.centralnic.net"},
            {"gb.net","whois.centralnic.net"},
            {"hu.com","whois.centralnic.net"},
            {"no.com","whois.centralnic.net"},
            {"qc.com","whois.centralnic.net"},
            {"ru.com","whois.centralnic.net"},
            {"sa.com","whois.centralnic.net"},
            {"se.com","whois.centralnic.net"},
            {"se.net","whois.centralnic.net"},
            {"uk.com","whois.centralnic.net"},
            {"uk.net","whois.centralnic.net"},
            {"us.com","whois.centralnic.net"},
            {"uy.com","whois.centralnic.net"},
            {"za.com","whois.centralnic.net"},
            {"jpn.com","whois.centralnic.net"},
            {"web.com","whois.centralnic.net"},
            {"eu.org","whois.eu.org"},
            {"com","whois.crsnic.net"},
            {"net","whois.crsnic.net"},
            {"org","whois.publicinterestregistry.net"},
            {"edu","whois.educause.net"},
            {"gov","whois.nic.gov"},
            {"int","whois.iana.org"},
            {"mil","whois.nic.mil"},
            {"aero","whois.information.aero"},
            {"biz","whois.nic.biz"},
            {"cat","whois.cat"},
            {"coop","whois.nic.coop"},
            {"info","whois.afilias.info"},
            {"jobs","whois.jobs"},
            {"mobi","whois.dotmobiregistry.net"},
            {"museum","whois.museum"},
            {"name","whois.nic.name"},
            {"pro","whois.registrypro.pro"},
            {"travel","whois.nic.travel"},
            {"ac","whois.nic.ac"},
            {"ae","whois.uaenic.ae"},
            {"af","whois.nic.af"},
            {"ag","whois.nic.ag"},
            {"am","whois.nic.am"},
            {"as","whois.nic.as"},
            {"at","whois.nic.at"},
            {"au","whois.ausregistry.net.au"},
            {"be","whois.dns.be"},
            {"bg","whois.register.bg"},
            {"bj","whois.nic.bj"},
            {"br","whois.nic.br"},
            {"bz","whois.belizenic.bz"},
            {"co.ca","whois.co.ca"},
            {"ca","whois.cira.ca"},
            {"cd","whois.nic.cd"},
            {"ch","whois.nic.ch"},
            {"ci","www.nic.ci"},
            {"ck","whois.nic.ck"},
            {"cl","whois.nic.cl"},
            {"edu.cn","whois.edu.cn"},
            {"cn","whois.cnnic.net.cn"},
            {"uk.co","whois.uk.co"},
            {"cx","whois.nic.cx"},
            {"cz","whois.nic.cz"},
            {"de","whois.denic.de"},
            {"dj","whois.domain.dj"},
            {"dk","whois.dk-hostmaster.dk"},
            {"dm","whois.nic.dm"},
            {"ee","whois.eenet.ee"},
            {"eu","whois.eu"},
            {"fi","whois.ficora.fi"},
            {"fj","whois.usp.ac.fj"},
            {"fo","whois.ripe.net"},
            {"fr","whois.nic.fr"},
            {"gd","whois.adamsnames.tc"},
            {"gf","whois.nplus.gf"},
            {"gg","whois.channelisles.net"},
            {"gm","whois.ripe.net"},
            {"gp","whois.nic.gp"},
            {"gs","whois.nic.gs"},
            {"hk","whois.hkdnr.net.hk"},
            {"hm","whois.registry.hm"},
            {"hu","whois.nic.hu"},
            {"id","whois.idnic.net.id"},
            {"ie","whois.domainregistry.ie"},
            {"il","whois.isoc.org.il"},
            {"in","whois.registry.in"},
            {"io","whois.nic.io"},
            {"ir","whois.nic.ir"},
            {"is","whois.isnet.is"},
            {"it","whois.nic.it"},
            {"je","whois.channelisles.net"},
            {"jp","whois.jprs.jp"},
            {"ke","whois.kenic.or.ke"},
            {"kg","whois.domain.kg"},
            {"kr","whois.nic.or.kr"},
            {"kz","whois.nic.kz"},
            {"la","whois.nic.la"},
            {"li","whois.nic.li"},
            {"lk","whois.nic.lk"},
            {"lt","whois.domreg.lt"},
            {"lu","whois.dns.lu"},
            {"lv","whois.nic.lv"},
            {"mc","whois.ripe.net"},
            {"mm","whois.nic.mm"},
            {"mn","whois.nic.mn"},
            {"ms","whois.adamsnames.tc"},
            {"mx","whois.nic.mx"},
            {"my","whois.mynic.net.my"},
            {"na","whois.na-nic.com.na"},
            {"nc","whois.cctld.nc"},
            {"nf","whois.nic.nf"},
            {"nl","whois.domain-registry.nl"},
            {"no","whois.norid.no"},
            {"nu","whois.nic.nu"},
            {"nz","whois.srs.net.nz"},
            {"pe","whois.nic.pe"},
            {"co.pl","whois.co.pl"},
            {"pl","whois.dns.pl"},
            {"pm","whois.nic.fr"},
            {"pt","whois.dns.pt"},
            {"pw","whois.nic.pw"},
            {"re","whois.nic.fr"},
            {"ro","whois.rotld.ro"},
            {"edu.ru","whois.informika.ru"},
            {"ru","whois.ripn.net"},
            {"sa","saudinic.net.sa"},
            {"sb","whois.nic.net.sb"},
            {"se","whois.nic-se.se"},
            {"sg","whois.nic.net.sg"},
            {"sh","whois.nic.sh"},
            {"si","whois.arnes.si"},
            {"sk","whois.sk-nic.sk"},
            {"sm","whois.ripe.net"},
            {"sr","whois.register.sr"},
            {"st","whois.nic.st"},
            {"su","whois.ripn.net"},
            {"tc","whois.adamsnames.tc"},
            {"tf","whois.nic.tf"},
            {"th","whois.thnic.net"},
            {"tj","whois.nic.tj"},
            {"tk","whois.dot.tk"},
            {"tl","whois.nic.tl"},
            {"tm","whois.nic.tm"},
            {"to","whois.tonic.to"},
            {"tp","whois.nic.tp"},
            {"tr","whois.metu.edu.tr"},
            {"tv","whois.nic.tv"},
            {"tw","whois.twnic.net"},
            {"ua","whois.net.ua"},
            {"ug","www.registry.co.ug"},
            {"gov.uk","whois.ja.net"},
            {"ac.uk","whois.ja.net"},
            {"uk","whois.nic.uk"},
            {"fed.us","whois.nic.gov"},
            {"us","whois.nic.us"},
            {"uy","www.rau.edu.uy"},
            {"uz","whois.cctld.uz"},
            {"va","whois.ripe.net"},
            {"vc","whois.opensrs.net"},
            {"ve","whois.nic.ve"},
            {"vg","whois.adamsnames.tc"},
            {"wf","whois.nic.wf"},
            {"ws","whois.samoanic.ws"},
            {"yt","whois.nic.yt"},
            {"ac.za","whois.ac.za"},
            {"gov.za","whois.gov.za"}
        };
    }
}
